import React from "react";
import FullNameDisplay from "./FullNameDisplay";
import "./styles.css";

const App = () => {
  return (
    <>
      <FullNameDisplay />
    </>
  );
};

export default App;
