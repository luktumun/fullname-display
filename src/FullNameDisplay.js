import React, { useEffect, useRef, useState } from "react";
import "./FullNameDisplay.css";

const FullNameDisplay = () => {
  const [fullName, setfullName] = useState("");
  const [lname, setLname] = useState("");
  const [fname, setFname] = useState("");
  const ref = useRef();

  const handleSubmit = (e) => {
    e.preventDefault();
    //...
    if (!fname) {
      alert("Enter First Name");
    }
    if (!lname) {
      alert("Enter Last Name");
    }
    if (fname && lname) {
      setfullName("Full Name: " + fname + " " + lname);
      ref.current = fullName;
    }
  };

  return (
    <form className="my_page" onSubmit={handleSubmit}>
      <h1>Full Name Display </h1>
      <label>
        First Name:
        <input
          type="text"
          value={fname}
          onChange={(e) => setFname(e.target.value)}
        />
      </label>

      <label>
        Last Name:
        <input
          type="text"
          value={lname}
          onChange={(e) => setLname(e.target.value)}
        />
      </label>

      <button type="submit">Submit</button>

      <h1>{fullName}</h1>
    </form>
  );
};

export default FullNameDisplay;
